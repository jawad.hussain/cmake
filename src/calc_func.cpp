#include "calculator.h"

void calc::get()
{
    cout << "Enter First Number: ";
    cin >> num1;
    cout << "Enter Second Number: ";
    cin >> num2;
};

double calc::add()
{
    return num1 +num2;
};

double calc::divide()
{
    return num1/num2;
};

double calc::multiply()
{
    return num1 * num2;
};

double calc::subtract()
{
    return num1 - num2;
};