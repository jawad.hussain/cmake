# CMake Tasks

## Task 1:
- A simple hello_world file was created and compiled using `CMake` command `add_executable(<target> <source>)`.

## Task 2:
- Imported the calculator cpp and header file from gcc project.
- Created a Static library using command `add_library(<target> <source>)`.

## Task 3:
- Link the static library using the command `target_link_libraries(<target> <source>)`.

## Task 4:
- Used the following commands to either build in release or debug mode:
    `set(CMAKE_BUILD_TYPE Release)`
    `set(CMAKE_BUILD_TYPE Debug)`

## Task 5:
- Enabled Warnings using the command `target_compile_options(<target> <Public/Private> <options>)`.

## Task 6:
- Pushed to repo.

## Task 7:
- used `.gitignore` to ignore build file.
